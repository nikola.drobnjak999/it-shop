<?php
$msg = isset($msg) ? $msg : "";
if (!isset($_SESSION)) session_start();
require_once '../login-page/DAO-login.php';
$daouser = new DAO();
$users = $daouser->selectAll();
$articles = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="../shop-page/shop.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container cards">
        <h1>Users</h1>
        <table class="table">
            <tr>
                <th>Type</th>
                <th>Username</th>
                <th>Password</th>
                <th>ID User Attribute</th>
                <th>First Name</th>
                <th>Last name</th>
            </tr>
            <?php foreach ($users as $pom) { ?>
                <tr>
                    <td><?= $pom['type']  ?></td>
                    <td><?= $pom['username']  ?></td>
                    <td><?= $pom['password']  ?></td>
                    <td><?= $pom['id_user_attributes']  ?></td>
                    <td><?= $pom['first_name']  ?></td>
                    <td><?= $pom['last_name']  ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>


    <script>
        var products =
            <?php echo json_encode($products);

            ?>;
    </script>