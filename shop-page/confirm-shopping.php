<?php
$msg = isset($msg) ? $msg : "";
require_once 'DAO-shop.php';
$dao = new DAOSHOP();
$products = $dao->selectProducts();
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="../shop-page/shop.css">
<title>Shop</title>
</head>
<body>
  <?php include_once '../partials/nav.php' ?>
  <?php include_once '../partials/header.php' ?>
  <div class="container cards">
    <form action="../shop-page/cart-controller.php" method="POST">
      First name: <br><input type="text" name="first_name"><br>
      Last name: <br><input type="text" name="last_name"><br>
      Phone number: <br><input type="text" name="phone_number"><br>
      Address: <br><input type="text" name="address"><br>
      City: <br><input type="text" name="city"><br>
      Type of delivery: <br>
      Taking packets in the post: <input type="radio" name="delivery" value="Post"><br>
      Delivery of the package to the address: <input type="radio" name="delivery" value="Address"><br>
      <input type="submit" name="action" value="Confirm">
    </form>
  </div>
  <?php include_once '../partials/bottom.php' ?>
  <?php include_once '../partials/footer.php' ?>
  <script>
    var products =
      <?php echo json_encode($products);

      ?>;
  </script>