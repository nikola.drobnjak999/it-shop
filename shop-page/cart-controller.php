<?php

require_once '../shop-page/DAO-shop.php';
require_once '../orders-page/DAO-orders.php';
session_start();
$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "";


if ($_SERVER['REQUEST_METHOD'] = "POST") {
    if ($action == 'Confirm') {
        $first_name = isset($_POST["first_name"]) ? test_input($_POST["first_name"]) : "";
        $last_name = isset($_POST["last_name"]) ? test_input($_POST["last_name"]) : "";
        $email = isset($_POST["email"]) ? test_input($_POST["email"]) : "";
        $phone_number = isset($_POST["phone_number"]) ? test_input($_POST["phone_number"]) : "";
        $address = isset($_POST["address"]) ? test_input($_POST["address"]) : "";
        $city = isset($_POST["city"]) ? test_input($_POST["city"]) : "";
        $delivery = isset($_POST["delivery"]) ? test_input($_POST["delivery"]) : "";
        $dao = new DAO();
        $dao->insertOrderDetails($first_name, $last_name, $phone_number, $address, $city,$delivery);
        $msg = "Order successfully placed!";
        include '../shop-page/cart.php';
    } elseif ($action == 'a') {
    }
}
if ($_SERVER['REQUEST_METHOD'] = "GET") {

    if ($action === 'logout') {
        session_unset();
        session_destroy();

        include '../shop-page/shop.php';
    } elseif ($action == 'addToCart') {

        $article = isset($_GET["article"]) ? unserialize($_GET["article"]) : "";

        if (!isset($_SESSION)) session_start();

        if (!isset($_SESSION['cart'])) $_SESSION['cart'] = [];

        $_SESSION['cart'][] = $article;
        include_once '../products-page/product.php';
        die();
    } elseif ($action == 'removeFromCart') {

        $article = isset($_GET["article"]) ? unserialize($_GET["article"]) : "";
        if (!isset($_SESSION)) session_start();
        if (!isset($_SESSION['cart'])) $_SESSION['cart'] = [];

        if (($key = array_search($article, $_SESSION['cart'])) !== false) {
            unset($_SESSION['cart'][$key]);
        }

        $msg = 'Article successfully deleted!';
        include_once '../shop-page/cart.php';
    } elseif ($action == 'emptyCart') {
        if (!isset($_SESSION)) session_start();
        unset($_SESSION['cart']);

        $msg = 'Korpa je obrisana';
        header('Location: cart.php');
    }
} else {
    die();
}
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
