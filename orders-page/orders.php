<?php
require_once 'DAO-orders.php';
$dao = new DAO();
$orders = $dao->selectOrders();
$msg = isset($msg) ? ($msg) : "";
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="orders.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <tr>
                        <th>ID Order</th>
                        <th>Quantity</th>
                        <th>View order</th>
                        <th>Delete order</th>
                        <th>Update order</th>
                    </tr>
                    <?php foreach ($orders as $pom) { ?>
                        <tr>
                            <td><?= $pom['order_id']  ?></td>
                            <td><?= $pom['quantity']  ?></td>
                            <td><a href="order-controller.php?action=view&order_id=<?= $pom['order_id'] ?>">View order</a></td>
                            <td><a href="order-controller.php?action=delete&order_id=<?= $pom['order_id'] ?>">Delete order</a></td>
                            <td><a href="update-order.php">Update order</a></td>

                        </tr>
                    <?php } ?>
                </table>
                <a href="insert-order.php">Add new order</a>
            </div>
        </div>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>