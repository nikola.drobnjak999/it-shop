<?php
require_once '../database/db.php';

class DAO
{
	private $db;
	private $INSERUSERWITHATTRIBUTES = "INSERT INTO user_attributes (first_name,last_name,email,phone_number) values (?,?,?,?)";
	private $INSERTUSER = "INSERT INTO users (type,username,password,id_user_attributes) values ('buyer',?,?,?)";
	private $SELECTBYUSERNAME = "SELECT * FROM users WHERE password = ?";
	private $SELECTALL = "SELECT * FROM users";
	private $SELECTBYUSERNAMEANDPASSWORD = "SELECT * FROM users JOIN user_attributes ON users.id_user_attributes=user_attributes.id_user_attribute WHERE users.username = ? AND users.password = ?";
	private $SELECTPRODUCTS = "SELECT * FROM products ORDER BY id ASC";
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	private function insertUserAttributes($first_name, $last_name, $email, $phone_number)
	{
		$statement = $this->db->prepare($this->INSERUSERWITHATTRIBUTES);
		$statement->bindValue(1, $first_name);
		$statement->bindValue(2, $last_name);
		$statement->bindValue(3, $email);
		$statement->bindValue(4, $phone_number);
		$statement->execute();
		return $this->db->lastInsertID();
	}

	private function insertUser($username, $password, $id_user_attributes)
	{
		$statement = $this->db->prepare($this->INSERTUSER);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		$statement->bindValue(3, $id_user_attributes);
		$statement->execute();
	}

	public function insertUserWithAtributes($first_name, $last_name, $email, $username, $password, $phone_number)
	{
		try {
			$this->db->beginTransaction();
			$id_user_atributes  = $this->insertUserAttributes($first_name, $last_name, $email, $phone_number);
			$this->insertUser($username, $password, $id_user_atributes);
			$this->db->commit();
			return true;
		} catch (PDOException $e) {
			$this->db->rollback();
			return false;
		}
	}

	public function selectUserByUsername($username)
	{
		$statement = $this->db->prepare($this->SELECTBYUSERNAME);
		$statement->bindValue(1, $username);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}

	public function selectUserByUsernameAndPassword($username, $password)
	{
		$statement = $this->db->prepare($this->SELECTBYUSERNAMEANDPASSWORD);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}

	public function selectAll()
	{
		$statement = $this->db->prepare($this->SELECTALL);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectAllProducts()
	{
		$statement = $this->db->prepare($this->SELECTPRODUCTS);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
}
